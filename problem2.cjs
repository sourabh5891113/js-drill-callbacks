const fs = require("fs");

function problem2() {
  //readind lipsum file
  fs.readFile("../lipsum_1.txt", "utf-8", (err, data) => {
    if (err) {
      console.error(err);
    } else {
      //converting data to uppercase
      let upperCaseData = data.toUpperCase();
      let upperCaseFileName = "upperCaseFile.txt";
      let fileNamesPath = "filenames.txt";
      //writing to uppercase file
      fs.writeFile(upperCaseFileName, upperCaseData, (err) => {
        if (err) {
          console.error(err);
        } else {
          console.log("Uppercase file written successfully");
          //writing to filename file
          fs.writeFile(fileNamesPath, upperCaseFileName + " ", (err) => {
            if (err) {
              console.error(err);
            }
          });
          //reading from uppercase file
          fs.readFile(upperCaseFileName, "utf-8", (err, data) => {
            if (err) {
              console.error(err);
            } else {
              //converting data to lowercase
              let lowerCaseData = data.toLowerCase();
              let sentences = lowerCaseData.split(".");
              let newSentences = sentences
                .map((sentence) => {
                  sentence = sentence.trim();
                  return sentence;
                })
                .filter((sentences) => sentences.length !== 0);
              let lowerCaseFileName = "lowerCaseFile.txt";
              //writing to filename file
              fs.writeFile(
                fileNamesPath,
                lowerCaseFileName + " ",
                { flag: "a" },
                (err) => {
                  if (err) {
                    console.error(err);
                  }
                }
              );
              //writing to lowercase file
              fs.writeFile(
                lowerCaseFileName,
                JSON.stringify(newSentences),
                (err) => {
                  if (err) {
                    console.error(err);
                  } else {
                    console.log("Lowercase file written successfully");
                    //reading from lowercase file
                    fs.readFile(lowerCaseFileName, "utf-8", (err, data) => {
                      if (err) {
                        console.error(err);
                      } else {
                        let sentences = JSON.parse(data);
                        sentences.sort();
                        let sortedFileName = "sortedFile.txt";
                        //writing to sorted file
                        fs.writeFile(
                          sortedFileName,
                          JSON.stringify(sentences),
                          (err) => {
                            if (err) {
                              console.error(err);
                            } else {
                              console.log("Sorted file written successfully");
                              fs.writeFile(
                                fileNamesPath,
                                sortedFileName,
                                { flag: "a" },
                                (err) => {
                                  if (err) {
                                    console.error(err);
                                  } else {
                                    //deleting all file files that are mentioned in filename file
                                    fs.readFile(
                                      fileNamesPath,
                                      "utf-8",
                                      (err, data) => {
                                        if (err) {
                                          console.error(err);
                                        } else {
                                          let filesArray = data.split(" ");
                                          for (
                                            let index = 0;
                                            index < filesArray.length;
                                            index++
                                          ) {
                                            const fileName = filesArray[index];
                                            fs.unlink(fileName, (err) => {
                                              if (err) {
                                                console.error(err);
                                              } else {
                                                console.log(
                                                  `${fileName} deleted successfully`
                                                );
                                              }
                                            });
                                          }
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                            }
                          }
                        );
                      }
                    });
                  }
                }
              );
            }
          });
        }
      });
    }
  });
}

module.exports = problem2;