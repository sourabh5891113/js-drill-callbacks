const fs = require("fs");
const path = require("path");

function createAndDeletefiles(directory, numberOfFiles) {
  if (typeof directory === "string" && typeof numberOfFiles === "number") {
    fs.mkdir(directory, { recursive: true }, (err) => {
      if (err) {
        console.error(err);
      } else {
        //creating file1
        let count = 1;
        function res(count) {
          if (count <= numberOfFiles) {
            let fileName1 = `file${count}.json`;
            let filePath1 = path.join(directory, fileName1);
            let fileData1 = {
              id: count,
              data: Math.random(),
            };
            fs.writeFile(filePath1, JSON.stringify(fileData1), (err) => {
              if (err) {
                console.error(err);
              } else {
                console.log(`${fileName1} created sucessfully`);

                //deleting file1
                fs.unlink(filePath1, (err) => {
                  if (err) {
                    console.error(err);
                  } else {
                    console.log(`${fileName1} deleted sucessfully`);
                    res(++count);
                  }
                });
              }
            });
          }
        }
        res(count);
      }
    });
  } else {
    console.log(
      "First argument should be a string and second should be a number"
    );
  }
}

module.exports = createAndDeletefiles;
