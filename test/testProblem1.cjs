/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const createAndDeletefiles = require("../problem1.cjs");

let directory = "randomJsonFiles";
let numberOfFiles = 5;
createAndDeletefiles(directory, numberOfFiles);
